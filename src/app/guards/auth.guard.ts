import { NavController } from '@ionic/angular';
import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Route, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
    private navControler: NavController
  ){}

  canActivate(): Promise<boolean>{
    return new Promise(resolve => {

      this.authService.getStorage("session_token").then( user=> {
        if(!user) this.navControler.navigateRoot('login');
        resolve(!!user);
      })
    });
  }
  
  
}
