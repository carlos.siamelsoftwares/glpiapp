import { NavController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
    private navController: NavController

  ){}

  canActivate(): Promise<boolean>{    
    return new Promise(resolve => {
      this.authService.getStorage("session_token").then( user=> {
        if(user) this.navController.navigateRoot('tabs/tab1');
        resolve(!user);
      })
    });
  }

}

