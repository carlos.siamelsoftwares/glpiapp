import { AuthService } from './../../services/auth.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonContent, NavParams, NavController } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-resposta-chamado',
    templateUrl: './resposta-chamado.page.html',
    styleUrls: ['./resposta-chamado.page.scss'],
})
export class RespostaChamadoPage implements OnInit {

    @ViewChild(IonContent) content: IonContent;
    @ViewChild('inputMsg') inputMsg: ElementRef;
    messages: Array<any> = [];
    messagesAll: Array<any>;
    solutions: any[] = null;
    currentUser: string;
    newMsg = '';
    id: number;
    tela: string;
    ticket: any;
    tasks: any;

    constructor(
        private keyboard: Keyboard,
        private activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private navCtrl: NavController
        ) {}

    ngOnInit() {
        this.id = this.activatedRoute.snapshot.params.id;
        this.tela = this.activatedRoute.snapshot.params.tela;
        this.currentUser = localStorage.getItem('usuario');
        this.loadAll();
    }

    sendMessage() {
       const content = {
           input: {
                itemtype: 'Ticket',
                is_private: 0,
                ticket_id: this.id,
                users_id: this.currentUser,
                content: this.newMsg,
                requesttypes_id: 1,
                use_notification: 1
            }
        };

       this.authService.requisicoes(`/Ticket/${this.id}/itilfollowup`, content).subscribe(data => {
            setTimeout(() => {
                this.content.scrollToBottom(300);
                this.newMsg = '';
            });
            console.log('enviado', data);
        });
    }

    async loadAll() {
        await this.loadMessages();
        await this.loadSolutions();
        // await this.loadTasks();
    }

    async loadMessages() {
        this.authService.requisicoes(`/Ticket/${this.id}/ITILFollowup/?expand_dropdowns=true&order=ASC`).subscribe( data => {
            data.forEach(element => {
                this.messages.push(element);
            });
        });
    }

    async  loadSolutions() {
        this.authService.requisicoes(`/Ticket/${this.id}/itilsolution/?expand_dropdowns=true&order=DESC`).subscribe( async data => {
            data.forEach(element => {
                this.messages.push(element);
            });
        });
    }

    async  loadTasks() {
        this.authService.requisicoes(`/Ticket/${this.id}/TicketTask/?expand_dropdowns=true&order=DESC`).subscribe( async data => {
            data.forEach(element => {
                environment.messages.push(element);
            });
        });
    }

    decode(text: string): string {
        return this.authService.decode(text);
    }

    doRefresh(event) {
        this.loadMessages().then(() => {
            event.target.complete();
        });
    }

    responder(id, tela) {
        this.navCtrl.navigateForward([id]);
    }

}
