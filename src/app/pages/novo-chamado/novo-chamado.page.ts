import { NavController } from '@ionic/angular';
import { LoadingService } from './../../services/loading.service';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-novo-chamado',
  templateUrl: './novo-chamado.page.html',
  styleUrls: ['./novo-chamado.page.scss'],
})
export class NovoChamadoPage implements OnInit {

  title: any;
  type: any;
  locations_id: any;
  content: any;
  requesttypes_id: any;
  myprofile: any;

  constructor(
    private authService: AuthService,
    private loading: LoadingService,
    private navCtrl: NavController) 
  { 

  }

  ngOnInit() {
    this.type = 1;
    this.requesttypes_id = 1;
    this.authService.requisicoes('/getActiveProfile').subscribe(data => {
      this.myprofile = data;
    });
  }

  async novoTicket(){
    await this.loading.presentLoading("Por favor, aguarde...");
    let content = {
      input: {
        name: this.title,
        type: this.type,
        locations_id: this.locations_id,
        content: this.content,
        requesttypes_id: this.requesttypes_id,
        _users_id_requester: this.myprofile.id
      }
    }
    
    this.authService.requisicoes('/Ticket',content).subscribe(data => {
      this.loading.loading.dismiss();
      this.navCtrl.navigateRoot("tabs/tab1");
    });
  }

}
