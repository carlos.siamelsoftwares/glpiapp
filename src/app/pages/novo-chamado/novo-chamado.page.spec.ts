import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoChamadoPage } from './novo-chamado.page';

describe('NovoChamadoPage', () => {
  let component: NovoChamadoPage;
  let fixture: ComponentFixture<NovoChamadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoChamadoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoChamadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
