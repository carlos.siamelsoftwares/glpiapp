import { LoadingService } from './../../services/loading.service';
import { AuthService } from 'src/app/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import {User} from '../../interfaces/user';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  public servidor: string;
  public protocolo: string;

  constructor(
      private authService: AuthService,
      private navControler: NavController,
      private loading: LoadingService
  ) { }

  ngOnInit() {
    this.authService.getStorage('servidor').then(data => {
      this.servidor = data;
    });

    this.authService.getStorage('protocolo').then(data => {
      this.protocolo = data;
    });
  }

  async saveUrl() {
    const user: User = {};

    user.servidor = this.servidor;
    await this.authService.saveStorage(user);
    await this.authService.saveStorage({protocolo: this.protocolo});
    await this.loading.presentLoading('Salvando...');
    await this.navControler.navigateRoot('login');
    this.authService.setDados();
    await this.loading.loading.dismiss();
    this.loading.presentToast('Configurações salvas!');

  }

}
