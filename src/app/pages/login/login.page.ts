import { HttpClient } from '@angular/common/http';
import { LoadingService } from './../../services/loading.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/interfaces/user';
import { Keyboard } from '@ionic-native/keyboard/ngx';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public avatar: string;
  public backgroundColor: string;
  public userLogin: User = {};
  public saldacoes: string;
  public lembrar = false;

  constructor(
    public keyboard: Keyboard,
    private authService: AuthService,
    private loadingService: LoadingService,
    private navControler: NavController
    ) { }

  ngOnInit() {
    this.avatar = 'assets/imagens/glpi.png';
    this.saldacoes = 'Seus chamados na palma de suas mãos!';
    console.log(this.lembrar);
  }

  async login() {
    const servidor = await this.authService.getStorage('servidor');
    if (servidor === null || servidor === '' || servidor === undefined) {
      await this.loadingService.presentToast('Por favor, configure o servidor primeiro');
    } else {
        await this.authService.login(this.userLogin, true, this.lembrar);
    }
  }

  check(event) {
    this.lembrar = !event.checked;
  }

  redirectSettinngs() {
    this.navControler.navigateRoot('settings');
  }

}
