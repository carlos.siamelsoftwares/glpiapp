import { NavController } from '@ionic/angular';
import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-details-chamado',
  templateUrl: './details-chamado.page.html',
  styleUrls: ['./details-chamado.page.scss'],
})
export class DetailsChamadoPage implements OnInit {

  id: string;
  ticket: any = {};
  segment: any;
  @ViewChild("reposta") reposta: HTMLElement;
 

  constructor(
      private activatedRoute: ActivatedRoute,
      private authService: AuthService,
      private navCtrl: NavController,
      private router: Router 
  ) { }

  selected(tipo,id){
      this.router.navigate(['finalizar',{id,tipo}]);
  }

  async ngOnInit() {
    this.id =  await this.activatedRoute.snapshot.params.id;
    this.getTicket();
  }

  async getTicket() {
    this.authService.requisicoes(`/Ticket/${this.id}`).subscribe(data => {
      this.ticket = data;
    });
  }

  decode(text: string): string {
    return this.authService.decode(text);
  }

  responder(id, tela) {
    this.navCtrl.navigateForward([tela, id]);
  }

  segmentChanged(ev) {
   this.segment = ev.detail.value;
  }
}
