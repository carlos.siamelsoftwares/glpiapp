import { RespostaChamadoPage } from './../resposta-chamado/resposta-chamado.page';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {AutosizeModule} from 'ngx-autosize';

import { IonicModule } from '@ionic/angular';

import { DetailsChamadoPage } from './details-chamado.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsChamadoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AutosizeModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [DetailsChamadoPage, RespostaChamadoPage]
})
export class DetailsChamadoPageModule {}
