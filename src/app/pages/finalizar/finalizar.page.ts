import { async } from '@angular/core/testing';
import { LoadingService } from './../../services/loading.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { NavController, IonContent } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-finalizar',
  templateUrl: './finalizar.page.html',
  styleUrls: ['./finalizar.page.scss'],
})
export class FinalizarPage implements OnInit {

  @ViewChild(IonContent) content: IonContent;
  @ViewChild('inputMsg') inputMsg: ElementRef;
  messages: Array<any> = [];
  messagesAll: Array<any>;
  solutions: any[] = null;
  currentUser: string;
  newMsg = '';
  id: number;
  tipo: string;
  ticket: any;
  tasks: any;
  title: string;
  path: string;
  
  constructor( 
    private keyboard: Keyboard,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private navCtrl: NavController,
    private loading: LoadingService) { }

  ngOnInit() {
    
    this.path = localStorage.getItem('protocolo') + '' + localStorage.getItem('servidor') + '/glpi';
    this.id = this.activatedRoute.snapshot.params.id;
    this.tipo = this.activatedRoute.snapshot.params.tipo;
    this.currentUser = localStorage.getItem('usuario');
    if(this.tipo == 'acompanhamento'){
      this.loadMessages();
      //this.loadDocuments();
      this.title = 'Acompanhamento';
    }else if( this.tipo == 'solucao'){
      this.loadSolutions();
      this.title = 'Solução';
    }else if( this.tipo == 'documento'){
       // this.loadDocuments();
        this.title = 'Documento';
    }
    this.messages = this.messages.sort(function(a, b) { return a.date_mod - b.date_mod; });
    console.log(this.messages);
  }


  async sendMessage() {
    await this.loading.presentLoading("Por favor, aguarde...");
    
    if(this.tipo == 'acompanhamento'){ 
        const content = {
            input: {
                itemtype: 'Ticket',
                is_private: 0,
                items_id: this.id,
                content: this.newMsg,
                requesttypes_id: 1
             }
        };

        await this.sendFollowup(content);
        this.loading.loading.dismiss();
    }else if(this.tipo == 'solucao'){

        const content = {
            input: {
                itemtype: 'Ticket',
                items_id: this.id,
                content: this.newMsg,
                solutiontypes_id: 1
             }
        };

        await this.sendSolution(content);
        this.loading.loading.dismiss();
    }

   
 }

 async sendFollowup(content: {}){
    this.authService.requisicoes(`/Ticket/${this.id}/itilfollowup`, content).subscribe(data => {
        setTimeout(() => {
            this.newMsg = '';
            this.content.scrollToBottom(300);
            this.messages = [];
            this.loadMessages();
        });
    });
 }

 async sendSolution(content: {}){
    this.authService.requisicoes(`/Ticket/${this.id}/itilsolution`, content).subscribe(data => {
        setTimeout(() => {
            this.content.scrollToBottom(300);
            this.newMsg = '';
            this.messages = [];
            this.loadSolutions();
            console.log(data);
        });
    });
 }

 async loadMessages() {
     this.authService.requisicoes(`/Ticket/${this.id}/ITILFollowup/?expand_dropdowns=true&order=ASC`).subscribe( data => {
        data.forEach(element => {
            this.messages.push(element);
        });
     });
 }

 async  loadSolutions() {
     this.authService.requisicoes(`/Ticket/${this.id}/itilsolution/?expand_dropdowns=true&order=DESC`).subscribe( async data => {
        data.forEach(element => {
            this.messages.push(element);
        });
     });
 }

//  async  loadTasks() {
//      this.authService.requisicoes(`/Ticket/${this.id}/TicketTask/?expand_dropdowns=true&order=DESC`).subscribe( async data => {
//         data.forEach(element => {
//             this.messages.push(element);
//         });
//      });
//  }

//  async  loadDocuments() {
//     this.authService.requisicoes(`/Ticket/${this.id}/?expand_dropdowns=true&with_documents=true`).subscribe( async data => {
//         if (this.authService.isNativeCall()) {
//             JSON.parse(data.data._documents).forEach(element => {
//                 this.messages.push(element);
//             });
//         } else {
//             data._documents.forEach(element => {
//                 this.messages.push(element);
//             });
//         }
//         console.log(data);
//     });
// }


 decode(text: string): string {
     return this.authService.decode(text);
 }

 doRefresh(event) {

     this.loadMessages().then(() => {
         event.target.complete();
     });
 }

 responder(id, tela) {
     this.navCtrl.navigateForward([id]);
 }

}
