import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {AutosizeModule} from 'ngx-autosize';

import { IonicModule } from '@ionic/angular';

import { FinalizarPage } from './finalizar.page';

const routes: Routes = [
  {
    path: '',
    component: FinalizarPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AutosizeModule
  ],
  declarations: [FinalizarPage]
})
export class FinalizarPageModule {}
