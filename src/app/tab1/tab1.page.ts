import { AuthService } from 'src/app/services/auth.service';
import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {

  public chamados: any = [];
  public vazio = false;
  public usuario: string;
  public myProfile: any = {};
  constructor(
    private authService: AuthService,
    private navCtrl: NavController
  ) {
  }

  decode(text: string): string {
    let maxLenght = 200;
    let reticencias = '';
    if( text.length > maxLenght ) reticencias = "...";
    return this.authService.decode(text.substring(0, maxLenght) +reticencias);
  }

  ngOnInit() {
    this.getMyProfiles();
    this.usuario = localStorage.getItem('usuario');
  }

  ionViewDidEnter(){
    this.chamados = [];
    this.getTickets();
  }

  getMyProfiles() {
    this.authService.getMyProfiles().subscribe(data => {
      this.myProfile = data.myprofiles[0].entities[0];
    });
  }

  async getTickets() {
    this.authService.getFullSession().subscribe(()=> {
      this.authService.requisicoes('/Ticket/?is_deleted=0&order=DESC').subscribe(async data => {
        data.filter(d => {
          if ( d.status === 2 || d.status === 3 ) {
            this.chamados.push(d);
          }
        });
        console.log(this.chamados);
      });
    },
    async err => {
      if (err.error[0] === 'ERROR_SESSION_TOKEN_INVALID') {

        const usuario = localStorage.getItem('usuario');
        const senha = localStorage.getItem('senha');

        await this.authService.login({usuario, senha}, true).then(() => {
          this.authService.requisicoes('/Ticket/').subscribe(data => {
            this.chamados = data;
          });
        });
      }

    }
    );

  }

  responder(id) {
    this.navCtrl.navigateForward(`details-chamado/${id}`);
  }

  doRefresh(event) {
    this.chamados = [];
    this.getTickets().then(() => {
      event.target.complete();
    });
  }

}
