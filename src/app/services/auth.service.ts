import {HTTP} from '@ionic-native/http/ngx';
import {LoadingService} from './loading.service';
import {NavController, Platform} from '@ionic/angular';
import {User} from './../interfaces/user';
import { Injectable, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public servidor: string;
  public loading: any;
  private session_token: string;
  private protocolo: string;
  private path = '/glpi/apirest.php';
  public dummyElem = document.createElement('DIV');


  constructor(
      private httpClient: HttpClient,
      private http: HTTP,
      private platform: Platform,
      private navControler: NavController,
      private loadingService: LoadingService
  ) {
    this.setDados();
  }

  setDados() {
    this.session_token = localStorage.getItem('session_token');
    this.protocolo = localStorage.getItem('protocolo');
    this.servidor = this.protocolo + localStorage.getItem('servidor') + this.path;
  }



  iniciarSessao(login: string): Observable<any> {

    let loginBase64 = btoa(login);

    let headers = { 'Content-Type': 'application/json', 'Authorization': 'Basic ' + loginBase64};
    let params = '/initSession';

    return this.getCall(headers, params);
  }

  fecharSessao(): Observable<any> {

    this.setDados();

    const headers = { 'Content-Type': 'application/json', 'Session-Token': this.session_token };
    const params = '/killSession';

    return this.getCall(headers, params);
  }

  requisicoes(params: string, content = null): Observable<any> {

    this.setDados();
    const headers = { 'Content-Type': 'application/json', 'Session-Token': this.session_token };

    return this.getCall(headers, params, content);
  }

  logout() {

    this.getFullSession().subscribe(data => {
      this.removeStorage('lembrar');
      this.removeStorage('session_token').then(() => {
        this.loadingService.presentLoading('Saindo...').then(() => {
          this.navControler.navigateRoot('login').then(() => {
            this.loadingService.loading.dismiss();
          });
        });
      });
    },
      err => {
        this.removeStorage('session_token').then(() => {
          this.removeStorage('lembrar');
          this.loadingService.presentLoading('Saindo...').then(() => {
            this.navControler.navigateRoot('login').then(() => {
              this.loadingService.loading.dismiss();
            });
          });
        });
      }
    );
  }

  async login(user: User, redirect: boolean = false, lembrar: boolean = null) {
    await this.loadingService.presentLoading('Por favor, aguarde...');
    this.iniciarSessao(user.usuario + ':' + user.senha).subscribe(async data => {
          user.session_token = data.session_token;
          this.saveStorage({ lembrar });
          this.saveStorage(user).then(() => {
              this.navControler.navigateRoot('tabs/tab1').then(() => {
                this.loadingService.loading.dismiss();
              });
          });
        }, async (err) => {
          await this.loadingService.loading.dismiss();
          this.loadingService.presentToast(err.error[1]);
        }
    );
  }

  public async saveStorage(obj: {} ) {

    for (const chave of Object.keys(obj)) {
      localStorage.setItem(chave, obj[chave]);
    }

  }

  public notLogin() {
    this.removeStorage('usuario');
    this.removeStorage('senha');
    this.removeStorage('usuario');
    this.removeStorage('lembrar');
    this.removeStorage('session_token');
  }

  public async removeStorage(key: string) {
    localStorage.removeItem(key);
  }


  public async getStorage(value: string) {
    return await localStorage.getItem(value);
  }


  getCall(headers: any, params: string, content = null) {

    this.setDados();

    if (content !== null) {
      return this.httpClient.post(this.servidor + params, content, { headers });
    } else {
      return this.httpClient.get(this.servidor + params, { headers });
    }

    // if (this.isNativeCall()) {
    //   if (content !== null) {
    //     return from( this.http.post(this.servidor + params, content, headers))
    //       .pipe(
    //           catchError(AuthService.handleError)
    //       );
    //   } else {
    //     return from( this.http.get(this.servidor + params, {} , headers))
    //       .pipe(
    //           catchError(AuthService.handleError)
    //       );
    //   }
    // } else {
    //   if (content !== null) {
    //     return this.httpClient.post(this.servidor + params, content, { headers })
    //     .pipe(
    //         catchError(AuthService.handleError)
    //     );
    //   } else {
    //     return this.httpClient.get(this.servidor + params, { headers })
    //     .pipe(
    //         catchError(AuthService.handleError)
    //     );
    //   }
    // }
  }

  public  getFullSession(): Observable<any> {
    return this.requisicoes('/getFullSession');
  }

  public getMyProfiles() {
    return this.requisicoes('/getMyProfiles');
  }

  // public isNativeCall() {
  //   if (!this.platform.is('cordova')) {
  //     return true;
  //   }
  //   return false;
  // }

  decode(text: string): string {
    let ret: string;

    this.dummyElem.innerHTML = text;
    document.body.appendChild(this.dummyElem);
    ret = this.dummyElem.textContent;
    document.body.removeChild(this.dummyElem);

    return ret;
  }

}
