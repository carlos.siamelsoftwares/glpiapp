import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  public loading: any;

  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController
    ) { }

  async presentLoading(message: string) {
    this.loading = await this.loadingController.create({message});
    return this.loading.present();  
  }

  async presentToast(message:string) {
    if(message && message.length < 5){
      message = "Houve um problema com a conexão ao servidor";
    }
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

}
