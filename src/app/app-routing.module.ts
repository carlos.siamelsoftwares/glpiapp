import { LoginGuard } from './guards/login.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

// const routes: Routes = [
//   { path: '', redirectTo: 'login', pathMatch: 'full' },
//   { path: 'home', loadChildren: './pages/home/home.module#HomePageModule',canActivate: [AuthGuard] },
//   { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule',canActivate: [LoginGuard] },
//   { path: 'details', loadChildren: './pages/details/details.module#DetailsPageModule',canActivate: [AuthGuard] },
//   { path: 'details/:id', loadChildren: './pages/details/details.module#DetailsPageModule',canActivate: [AuthGuard ] },
//   { path: 'chamados', loadChildren: './pages/chamados/chamados.module#ChamadosPageModule' },
//   { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
//   { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
//   { path: 'tab1', loadChildren: './tab1/tab1.module#Tab1PageModule' },
//   { path: 'tab2', loadChildren: './tab2/tab2.module#Tab2PageModule' }
// ];

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule', canActivate: [AuthGuard] },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule', canActivate: [LoginGuard] },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule'},
  { path: 'details-chamado', loadChildren: './pages/details-chamado/details-chamado.module#DetailsChamadoPageModule', canActivate: [AuthGuard] },
  { path: 'details-chamado/:id', loadChildren: './pages/details-chamado/details-chamado.module#DetailsChamadoPageModule',canActivate: [AuthGuard] },
  { path: 'resposta-chamado', loadChildren: './pages/resposta-chamado/resposta-chamado.module#RespostaChamadoPageModule', canActivate: [AuthGuard] },
  { path: 'resposta-chamado/:id', loadChildren: './pages/resposta-chamado/resposta-chamado.module#RespostaChamadoPageModule', canActivate: [AuthGuard] },
  { path: 'finalizar', loadChildren: './pages/finalizar/finalizar.module#FinalizarPageModule', canActivate: [AuthGuard] },
  { path: 'finalizar/:id', loadChildren: './pages/finalizar/finalizar.module#FinalizarPageModule', canActivate: [AuthGuard] },
  { path: 'novo-chamado', loadChildren: './pages/novo-chamado/novo-chamado.module#NovoChamadoPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule{ }