import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.page.html',
  styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {

  public chamados: any = [];
  public vazio = false;
  public usuario: string;
  public myProfile: any = {};
  constructor(
    private authService: AuthService,
    private navCtrl: NavController
  ) {
  }

  decode(text: string): string {
    let maxLenght = 200;
    let reticencias = '';
    if( text.length > maxLenght ) reticencias = "...";
    return this.authService.decode(text.substring(0, maxLenght) +reticencias);
  }

  ngOnInit() {
    this.getMyProfiles();
    this.usuario = localStorage.getItem('usuario');
  }

  ionViewDidEnter(){
    this.chamados = [];
    this.getTickets();
  }

  getMyProfiles() {
    this.authService.getMyProfiles().subscribe(data => {
      this.myProfile = data.myprofiles[0].entities[0];
    });
  }

  async getTickets() {
    this.authService.getFullSession().subscribe(()=> {
      console.log("teste");
      this.authService.requisicoes(`/Ticket?expand_dropdowns=true`).subscribe(async data => {
        console.log(data);
        const user = localStorage.getItem("usuario");
        data.filter(d => {
          if ( d.status === 6 && (user == d.users_id_recipient || user == 'glpi')) {
            this.chamados.push(d);
          }
        });
      });
    },
    async err => {
      if (err.error[0] === 'ERROR_SESSION_TOKEN_INVALID') {

        const usuario = localStorage.getItem('usuario');
        const senha = localStorage.getItem('senha');

        await this.authService.login({usuario, senha}, true).then(() => {
          this.authService.requisicoes("/Ticket").subscribe(data => {
            data.filter(d => {
              if ( d.status === 6 ) {
                this.chamados.push(d);
              }
            });
          });
        });
      }
    }
    ); 
  }

  responder(id) {
    this.navCtrl.navigateForward(`details-chamado/${id}`);
  }

  novoTicket(){
    console.log("teste");
    this.navCtrl.pop();
    this.navCtrl.navigateRoot("novo-chamado");
  }

  // doRefresh(event) {
  //   this.chamados = [];
  //   this.getTickets().then(() => {
  //     event.target.complete();
  //   });
  // }

}
