export interface User {
    usuario?: string;
    senha?: string;
    session_token?: string;
    servidor?: string;
}
